resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}
resource "helm_release" "prometheus" {
  name       = "prometheus"
  namespace  = "monitoring"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"
  version    = "59.0.0"

  values = [
    file("prometheus-values.yaml")
  ]

  set {
    name  = "grafana.enabled"
    value = "true"
  }

  set {
    name  = "grafana.adminPassword"
    value = var.grafana_password
   }
}


resource "helm_release" "blackbox_exporter" {
  name       = "blackbox-exporter"
  namespace  = "monitoring"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus-blackbox-exporter"
  version    = "8.17.0" 

 
}

data "local_file" "grafana_dashboard" {
  filename = "blackbox_dashboard.json"
}

resource "kubernetes_config_map" "grafana_dashboard" {
  metadata {
    name      = "grafana-dashboard"
    namespace = "monitoring"
  }

  data = {
    "grafana-dashboard.json" = data.local_file.grafana_dashboard.content
  }
}
