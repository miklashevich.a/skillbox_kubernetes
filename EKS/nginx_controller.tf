module "nginx-controller" {
  source  = "terraform-iaac/nginx-controller/helm"
  namespace = "default"

  additional_set = [
    {
      name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-type"
      value = "nlb"
      type  = "string"
    },
    {
      name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-cross-zone-load-balancing-enabled"
      value = "true"
      type  = "string"
    },
    {
      name  = "metrics_enabled"
      value = "true"
      type  = "string"
    }

  ]
}

output "ingress_class_name" {
  description = "iam role arn for EKS cluster"
  value       = module.nginx-controller.ingress_class_name
}