resource "kubernetes_namespace" "loki" {
  metadata {
    name = "loki"
  }
}


resource "helm_release" "loki" {
  name       = "loki"
  namespace  = "loki"
  repository = "https://grafana.github.io/helm-charts"
  chart      = "loki-stack"
  version    = "2.10.2"



  set {
    name  = "grafana.enabled"
    value = "true"
  }
 
}

resource "kubernetes_ingress_v1" "grafana_ingress" {
  metadata {
    name      = "grafana-ingress"
    namespace = "loki"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
    }
  }

  spec {
    rule {
      host = "logs.miklashevich.online"
      http {
        path {
          path = "/"
          path_type = "ImplementationSpecific"
          backend {
            service {
              name = "loki-grafana"
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}





data "external" "grafana_password" {
  program = ["bash", "-c", "echo -n '{\"password\": \"'$(kubectl get secret --namespace loki loki-grafana -o jsonpath='{.data.admin-password}' | base64 --decode)'\"}'"]
}

output "grafana_admin_password" {
  value = data.external.grafana_password.result["password"]
  sensitive = false
}



