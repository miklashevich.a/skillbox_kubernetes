variable "vpc_cidr" {
  description = "vpc cidr"
  type        = string

}

variable "region" {
  description = "aws region"
  type        = string

}

variable "private_subnets" {
  description = "private subnets CIDR"
  type        = list(string)
}

variable "public_subnets" {
  description = "public subnets CIDR"
  type        = list(string)
}

variable "cluster_name" {
  description = "Name of the EKS cluster"
  type        = string
}

variable "terraform_arn" {
  description = "ARN for terraform user"
  type        = string
}

variable "route53_zone_id" {
  description = "The ID of the hosted zone to contain this record."
  type        = string
}

variable "grafana_password" {
  description = "The password for Grafana"
  sensitive   = true
}