output "cluster_name" {
  description = "EKS cluster name"
  value       = module.eks.cluster_name
}


output "cluster_endpoint" {
  description = "EKS cluster endpoint"
  value       = module.eks.cluster_endpoint
}


output "cluster_security_group_id" {
  description = "Security Group ID for EKS cluster"
  value       = module.eks.cluster_security_group_id
}

output "cluster_iam_role_arn" {
  description = "iam role arn for EKS cluster"
  value       = module.eks.cluster_iam_role_arn
}





